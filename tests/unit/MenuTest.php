<?php

declare(strict_types=1);

namespace App\Tests\unit;

use App\Entity\Menu;
use PHPUnit\Framework\TestCase;

class MenuTest extends TestCase
{
    private Menu $menu;

    protected function setUp(): void
    {
        $this->menu = new Menu();
        $this->menu->setName('PHP');
        $this->menu->setContent('Some description');
        $this->menu->setIsVisible(true);
    }

    protected function tearDown(): void
    {
    }

    public function testGetName()
    {
        $this->assertEquals('PHP', $this->menu->getName());
    }

    public function testIsVisible()
    {
        $this->assertEquals(true, $this->menu->isIsVisible());
    }

    public function testGetContent()
    {
        $this->assertEquals('Some description', $this->menu->getContent());
    }
}
