--
-- PostgreSQL database dump
--

-- Dumped from database version 14.3
-- Dumped by pg_dump version 14.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: doctrine_migration_versions; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.doctrine_migration_versions (
    version character varying(191) NOT NULL,
    executed_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
    execution_time integer
);


ALTER TABLE public.doctrine_migration_versions OWNER TO "user";

--
-- Name: menu; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.menu (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    slug character varying(255) NOT NULL,
    parent_id integer,
    content text,
    is_visible boolean NOT NULL
);


ALTER TABLE public.menu OWNER TO "user";

--
-- Name: menu_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public.menu_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.menu_id_seq OWNER TO "user";

--
-- Data for Name: doctrine_migration_versions; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.doctrine_migration_versions (version, executed_at, execution_time) FROM stdin;
DoctrineMigrations\\Version20220530132524	2022-06-16 12:19:00	120
DoctrineMigrations\\Version20220530133141	2022-06-16 12:19:01	4
DoctrineMigrations\\Version20220530164457	2022-06-16 12:19:01	34
\.


--
-- Data for Name: menu; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.menu (id, name, slug, parent_id, content, is_visible) FROM stdin;
55	HTTP	http	\N	<div><strong>HTTP (HyperText Transfer Protocol):</strong></div><div>Протокол передачи гипертекста—протокол прикладного уровня передачи данных, изначально — в виде гипертекстовых документов в формате HTML, в настоящее время используется для передачи произвольных данных.</div><div><a href="https://ru.wikipedia.org/wiki/SOAP"><strong><em>SOAP</em></strong></a> <strong><em>— </em></strong>текстовый протокол на базе HTTP</div>	t
57	HTTPS	https	\N	<div>HTTPS (HyperText Transfer Protocol Secure):</div><div>Расширение протокола HTTPдля поддержки шифрованияв целях повышения безопасности.&nbsp;<br>Данные в протоколе HTTPS передаются поверх криптографических протоколов&nbsp;<br>TLS или устаревшего в 2015 году SSL<br><br></div>	t
56	Статусы	statusy	55	<div><strong><em>Статусы:</em></strong>&nbsp;</div><ul><li>Информационные 100-199 (пример102 "в обработке")&nbsp;</li><li>Успешные 200-299(пример 204 "нет содержимого", но заголовки присылаются)&nbsp;</li><li>Перенаправления 300-399(303 "Просмотр других ресурсов")</li><li>&nbsp;Клиентские ошибки 400-499(400 "Плохой запрос")&nbsp;</li><li>Серверные ошибки 500-599(500 "Внутренняя ошибка сервера")</li></ul>	t
59	Методы	metody	55	<div><strong><em>Методы HTTP запроса:</em></strong></div><ul><li><strong>GET</strong>- получение ресурса</li><li><strong>HEAD</strong> - запрашивает ресурс так же, как и метод GET, но без тела ответа.</li><li><strong>POST</strong>- создание ресурса</li><li><strong>PUT</strong>- обновление ресурса</li><li><strong>PATCH</strong>- используется для частичного изменения ресурса</li><li><strong>DELETE</strong>- удаление ресурса</li><li><strong>CONNECT</strong>- устанавливает "туннель" к серверу, определённому по ресурсу.</li><li><strong>OPTIONS</strong>- используется для описания параметров соединения с ресурсом.</li><li><strong>TRACE</strong>- выполняет вызов возвращаемого тестового сообщения с ресурса</li></ul>	t
58	REST	rest	\N	<div><strong>REST (Representational State Transfer):</strong><br>Передача состояния представления, другими словами, REST— это набор правил того, как программисту организовать написание кода серверного приложения, чтобы все системы легко обменивались данными и приложение можно было масштабировать</div>	t
60	API	api	58	<div><strong><em>API (A</em></strong><em>pplication </em><strong><em>P</em></strong><em>rogramming </em><strong><em>I</em></strong><em>nterface</em><strong><em>):</em></strong><br>Программный интерфейс приложения- описание способов (набор классов, процедур, функций, структур или констант), которыми одна компьютерная программа может взаимодействовать с другой программой.<br><br></div>	t
61	REST API	rest-api	58	<div><strong><em>REST API:</em></strong><br>Это когда серверное приложение дает доступ к своим данным клиентскому приложению по определенному URL. Далее разберем подробнее, начиная с базовых понятий.<br><br><br></div>	t
64	Область видимости	oblast-vidimosti	63	<div><a href="https://www.php.net/manual/ru/language.variables.scope.php">Область видимости переменной</a></div><div>Область видимости переменной - это контекст, в котором эта переменная определена. В большинстве случаев все переменные PHP имеют только одну область видимости. Эта единая область видимости охватывает также включаемые (include) и требуемые (require) файлы.</div>	t
65	Переменные переменных	peremennye-peremennykh	63	<div><a href="https://www.php.net/manual/ru/language.variables.variable.php">Переменные переменных</a></div><div>Иногда бывает удобно иметь переменными имена переменных. То есть, имя переменной, которое может быть определено и изменено динамически.</div><div>Переменная переменной берет значение переменной и рассматривает его как имя переменной.</div><div>$$a='world';</div>	t
66	Переменные извне PHP	peremennye-izvne-php	63	<div><a href="https://www.php.net/manual/ru/language.variables.external.php">Переменные извне PHP</a><br>Когда происходит отправка данных формы PHP-скрипту, информация из этой формы автоматически становится доступной ему. Существует несколько способов получения этой информации<br>Есть только два способа получить доступ к данным из форм HTML.<br><br>\techo $_POST['username'];<br><br>\techo $_REQUEST['username'];</div><div><br></div>	t
68	Предопределенные костанты	predopredelennye-kostanty	67	<div><a href="https://www.php.net/manual/ru/reserved.constants.php"><em>Предопределенные костанты</em></a><em><br></em><br></div><div>Эти константы объявляются ядром PHP</div><div><br></div>	t
69	Магические константы	magicheskie-konstanty	67	<div><a href="https://www.php.net/manual/ru/language.constants.magic.php">Магические константы</a><br>Есть девять магических констант, которые меняют своё значение в зависимости от контекста, в котором они используются. Например, значение __LINE__ зависит от строки в скрипте, на которой эта константа указана. Все магические константы разрешаются во время компиляции, в отличие от обычных констант, которые разрешаются во время выполнения</div><div><br></div>	t
67	Константы	konstanty	62	<div><strong><em>Различия между константами и переменными:</em></strong></div><div>У констант нет приставки в виде знака доллара ($);</div><div>Константы могут быть определены и доступны в любом месте без учёта области видимости;</div><div>Константы не могут быть переопределены или удалены после первоначального объявления;&nbsp;</div><div>Константы могут иметь только скалярные значения или массивы.<br>До PHP 8.0.0 константы, определённые с помощью функции <a href="https://www.php.net/manual/ru/function.define.php">define()</a>, могли быть нечувствительными к регистру<br>Константа может быть определена с помощью ключевого слова constили с помощью функции <a href="https://www.php.net/manual/ru/function.define.php">define()</a>. В то время как <a href="https://www.php.net/manual/ru/function.define.php">define()</a>позволяет задать константу через выражение, конструкция constограничена как описано в следующем параграфе. После того, как константа определена, её значение не может быть изменено или аннулировано.</div><div>При использовании ключевого слова const допускаются только скалярные выражения (bool, int, float и string) и константы array, содержащие только скалярные выражения.<br><br></div>	t
62	PHP	php	\N	<div><a href="https://www.php.net/manual/ru/intro-whatis.php">Что такое PHP? </a><strong><em><br></em></strong>PHP (рекурсивный акроним словосочетания PHP: Hypertext Preprocessor) - это распространённый язык программирования общего назначения с открытым исходным кодом. PHP специально сконструирован для веб-разработок и его код может внедряться непосредственно в HTML.<br><br>PHP входит в число языков с <strong>динамической</strong> типизацией. Это значит, что тип данных определяется не при объявлении переменной, а при присваивании значения<br><br>PHP — <strong>интерпретируемый язык</strong>. Написанные на нём программы интерпретируются в момент обращения с помощью специальных программ. Интерпретируемые языки не зависят от платформы, но уступают компилируемым языкам в скорости исполнения.</div><div><br></div><div><br></div>	t
70	Типы данных PHP	tipy-dannykh-php	62	<div><a href="https://www.php.net/manual/ru/language.types.php">Типы данных</a><br><br>PHP поддерживает десять простых типов.</div><div>Четыре скалярных типа:</div><ul><li>bool</li><li>int</li><li>float (число с плавающей точкой, также известное как double)</li><li>string</li></ul><div>Четыре смешанных типа:<br><br></div><ul><li>array</li><li>object</li><li><a href="https://www.php.net/manual/ru/language.types.callable.php">callable</a></li><li><a href="https://www.php.net/manual/ru/language.types.iterable.php">iterable</a></li></ul><div>И, наконец, два специальных типа:<br><br></div><ul><li>resource</li><li>NULL</li></ul><div>Вы также можете найти несколько упоминаний типа двойной точности (double). Рассматривайте его как число с плавающей точкой, два имени существуют только по историческим причинам.<br><br></div><div>Как правило, программист не устанавливает тип переменной; обычно это делает PHP во время выполнения программы в зависимости от контекста, в котором используется переменная.<br><br></div>	t
71	Предопределённые переменные	predopredelionnye-peremennye	63	<div><a href="https://www.php.net/manual/ru/language.variables.predefined.php#language.variables.predefined">Предопределённые переменные <br></a>Любому запускаемому скрипту PHP предоставляет большое количество предопределённых переменных. Однако многие из этих переменных не могут быть полностью задокументированы, поскольку они зависят от запускающего скрипт сервера, его версии и настроек, а также других факторов. Некоторые из этих переменных недоступны, когда PHP запущен из <a href="https://www.php.net/manual/ru/features.commandline.php">командной строки</a>. Смотрите <a href="https://www.php.net/manual/ru/reserved.variables.php">список зарезервированных предопределённых переменных</a> для получения дополнительной информации.</div>	t
72	Суперглобальные переменные	superglobalnye-peremennye	63	<div><a href="https://www.php.net/manual/ru/language.variables.superglobals.php">Суперглобальные переменные</a><br>Суперглобальные переменные — Встроенные переменные, которые всегда доступны во всех областях<br>Некоторые предопределённые переменные в PHP являются "суперглобальными", что означает, что они доступны в любом месте скрипта. Нет необходимости использовать синтаксис <strong>global $variable;</strong> для доступа к ним в функциях и методах.<br><br></div><div>Суперглобальными переменными являются:<br><br></div><ul><li><a href="https://www.php.net/manual/ru/reserved.variables.globals.php"><em>$GLOBALS</em></a></li><li><a href="https://www.php.net/manual/ru/reserved.variables.server.php"><em>$_SERVER</em></a></li><li><a href="https://www.php.net/manual/ru/reserved.variables.get.php"><em>$_GET</em></a></li><li><a href="https://www.php.net/manual/ru/reserved.variables.post.php"><em>$_POST</em></a></li><li><a href="https://www.php.net/manual/ru/reserved.variables.files.php"><em>$_FILES</em></a></li><li><a href="https://www.php.net/manual/ru/reserved.variables.cookies.php"><em>$_COOKIE</em></a></li><li><a href="https://www.php.net/manual/ru/reserved.variables.session.php"><em>$_SESSION</em></a></li><li><a href="https://www.php.net/manual/ru/reserved.variables.request.php"><em>$_REQUEST</em></a></li><li><a href="https://www.php.net/manual/ru/reserved.variables.environment.php"><em>$_ENV</em></a></li></ul>	t
63	Переменные	peremennye	62	<div><a href="https://www.php.net/manual/ru/language.variables.php">Переменные</a> в PHP представлены знаком доллара с последующим именем переменной. Имя переменной чувствительно к регистру.<br><br></div><div><a href="https://www.php.net/manual/ru/language.variables.predefined.php">Предопределённые переменные</a><br><br></div><div><a href="https://www.php.net/manual/ru/language.variables.superglobals.php">Суперглобальные переменные</a> — Встроенные переменные, которые всегда доступны во всех областях<br><br></div><div><br></div><div><br></div>	t
73	Пространство имен	prostranstvo-imen	62	<div><a href="https://www.php.net/manual/ru/language.namespaces.php"><strong><em>Пространство имен(namespace)</em></strong></a><strong><em><br></em></strong>Что такое пространства имён? В широком смысле - это один из способов инкапсуляции элементов. Такое абстрактное понятие можно увидеть во многих местах. Например, в любой операционной системе директории служат для группировки связанных файлов и выступают в качестве пространства имён для находящихся в них файлов. В качестве конкретного примера файл foo.txt может находиться сразу в обеих директориях: /home/greg и /home/other, но две копии foo.txt не могут существовать в одной директории. Кроме того, для доступа к foo.txt извне директории /home/greg, мы должны добавить имя директории перед именем файла используя разделитель, чтобы получить /home/greg/foo.txt. Этот же принцип распространяется и на пространства имён в программировании.<br><br></div><div>В PHP пространства имён используются для решения двух проблем, с которыми сталкиваются авторы библиотек и приложений при создании повторно используемых элементов кода, таких как классы и функции:<br><br></div><ol><li>Конфликт имён между вашим кодом и внутренними классами/функциями/константами PHP или сторонними.</li><li>Возможность создавать псевдонимы (или сокращения) для Ну_Очень_Длинных_Имён, чтобы облегчить первую проблему и улучшить читаемость исходного кода.</li></ol><div>Пространства имён в PHP предоставляют возможность группировать логически связанные классы, интерфейсы, функции и константы<br><br></div>	t
74	Булев	bulev	70	<div><a href="https://www.php.net/manual/ru/language.types.boolean.php">Булев</a><br>Это простейший тип. bool выражает истинность значения. Он может быть либо <strong>true</strong>, либо <strong>false</strong>.<br><br></div>	t
76	SQL	sql	\N	\N	t
78	SOLID	solid	\N	\N	t
77	PHP ООП	php-oop	\N	<div>PHP включает полноценную объектную модель. Некоторые из её особенностей: <a href="https://www.php.net/manual/ru/language.oop5.visibility.php">видимость</a>, <a href="https://www.php.net/manual/ru/language.oop5.abstract.php">абстрактные</a> и <a href="https://www.php.net/manual/ru/language.oop5.final.php">ненаследуемые</a> (final) классы и методы, а также <a href="https://www.php.net/manual/ru/language.oop5.magic.php">магические методы</a>, <a href="https://www.php.net/manual/ru/language.oop5.interfaces.php">интерфейсы</a> и <a href="https://www.php.net/manual/ru/language.oop5.cloning.php">клонирование</a>.<br><br></div><div>PHP работает с объектами так же, как с ссылками или дескрипторами, это означает что каждая переменная содержит ссылку на объект, а не его копию. Более подробную информацию смотрите в разделе <a href="https://www.php.net/manual/ru/language.oop5.references.php">Объекты и ссылки</a>.<br><br></div>	t
82	INT	int	75	<div>Целые числа (int) могут быть указаны в десятичной (основание 10), шестнадцатеричной (основание 16), восьмеричной (основание 8) или двоичной (основание 2) системе счисления. Для задания отрицательных целых (int) используется <a href="https://www.php.net/manual/ru/language.operators.arithmetic.php">оператор отрицания<br></a><br></div><div>Для записи в восьмеричной системе счисления, необходимо поставить перед числом 0 (ноль). Начиная с PHP 8.1.0, восьмеричной нотации также может предшествовать 0o или 0O. Для записи в шестнадцатеричной системе счисления, необходимо поставить перед числом 0x. Для записи в двоичной системе счисления, необходимо поставить перед числом 0b<br><br></div><div>Начиная с PHP 7.4.0, целочисленные литералы могут содержать подчёркивания (_) между цифрами для лучшей читаемости литералов. Эти подчёркивания удаляются сканером PHP.</div>	t
75	Целые числа	tselye-chisla	70	<div><a href="https://www.php.net/manual/ru/language.types.integer.php">Целые числа</a><br>int - это число из множества ℤ = {..., -2, -1, 0, 1, 2, ...}.<br><br></div><div>Смотрите также:<br><br></div><ul><li><a href="https://www.php.net/manual/ru/book.gmp.php">Вычисления над целыми числами с произвольной точностью (GNU Multiple Precision)</a></li><li><a href="https://www.php.net/manual/ru/language.types.float.php">Числа с плавающей точкой</a></li><li><a href="https://www.php.net/manual/ru/book.bc.php">Вычисления над числами с произвольной точностью BCMath</a></li></ul><div>Синтаксис...&gt;<a href="https://www.php.net/manual/ru/language.types.integer.php#language.types.integer.syntax"><br></a><br></div><div><br></div>	t
95	Callback-функции	callback-funktsii	70	<div><a href="https://www.php.net/manual/ru/language.types.callable.php">Функции обратного вызова (callback-функции)</a><br>Callback-функции могут быть обозначены объявлением типа <a href="https://www.php.net/manual/ru/language.types.callable.php">callable</a>.<br><br></div><div>Некоторые функции, такие как <a href="https://www.php.net/manual/ru/function.call-user-func.php">call_user_func()</a> или <a href="https://www.php.net/manual/ru/function.usort.php">usort()</a>, принимают определённые пользователем callback-функции в качестве параметра. Callback-функции могут быть как простыми функциями, так и методами объектов, включая статические методы классов.<br><br></div><div><br></div><div><br></div>	t
79	KISS, DRY, YAGNI	kiss-dry-yagni	\N	<div><strong>YAGNI</strong></div><div><em>You Aren’t Gonna Need It / Вам это не понадобится<br></em><strong>DRY</strong></div><div><em>Don’t Repeat Yourself / Не повторяйтесь<br></em><strong>KISS</strong></div><div><em>Keep It Simple, Stupid / Будь проще</em></div>	t
84	DOKER	doker	\N	\N	t
85	COMPOSER	composer	\N	\N	t
83	ПАТТЕРНЫ	patterny	\N	\N	t
81	Float, double	float-double	70	<div><a href="https://www.php.net/manual/ru/language.types.float.php">Числа с плавающей точкой </a>или числа с плавающей запятой (также известные как "float", "double" или "real")&nbsp;</div>	t
86	Строки	stroki	70	<div><a href="https://www.php.net/manual/ru/language.types.string.php">Строки</a><br>Строка (тип string) - это набор символов, где символ - это то же самое, что и байт. Это значит, что PHP поддерживает ровно 256 различных символов, а также то, что в PHP нет встроенной поддержки Unicode. Смотрите также <a href="https://www.php.net/manual/ru/language.types.string.php#language.types.string.details">подробности реализации строкового типа</a>.<br>Строка может быть определена четырьмя различными способами:<br><br></div><ul><li><a href="https://www.php.net/manual/ru/language.types.string.php#language.types.string.syntax.single">одинарными кавычками</a></li><li><a href="https://www.php.net/manual/ru/language.types.string.php#language.types.string.syntax.double">двойными кавычками</a></li><li><a href="https://www.php.net/manual/ru/language.types.string.php#language.types.string.syntax.heredoc">heredoc-синтаксисом</a></li><li><a href="https://www.php.net/manual/ru/language.types.string.php#language.types.string.syntax.nowdoc">nowdoc-синтаксисом</a></li></ul><div><br></div><div><br><br></div>	t
87	Числовые строки	chislovye-stroki	70	<div><a href="https://www.php.net/manual/ru/language.types.numeric-strings.php">Числовые строки</a><br>В PHP, строка (string) считается числовой, если её можно интерпретировать как целое (int) число или как число с плавающей точкой (float).<br><br></div><div>Формально с PHP 8.0.0:<br><br></div><pre>WHITESPACES      \\s*\r\nLNUM             [0-9]+\r\nDNUM             ([0-9]*)[\\.]{LNUM}) | ({LNUM}[\\.][0-9]*)\r\nEXPONENT_DNUM    (({LNUM} | {DNUM}) [eE][+-]? {LNUM})\r\nINT_NUM_STRING   {WHITESPACES} [+-]? {LNUM} {WHITESPACES}\r\nFLOAT_NUM_STRING {WHITESPACES} [+-]? ({DNUM} | {EXPONENT_DNUM}) {WHITESPACES}\r\nNUM_STRING       ({INT_NUM_STRING} | {FLOAT_NUM_STRING})\r\n<br></pre><div>В PHP также присутствует концепция <em>префиксной</em> числовой строки. Это строка, которая начинается как числовая и продолжается любыми другими символами.<br><br></div><div>Строки, используемые в числовых контекстах<a href="https://www.php.net/manual/ru/language.types.numeric-strings.php#language.types.numeric-string.conversion"> ¶<br></a><br></div><div>Когда строку необходимо использовать в качестве числа (например арифметические операции, декларация целочисленного типа, и т.д.), используется следующий алгоритм действий:<br><br></div><ol><li>Если строка числовая, представляет целое число и не превышает максимально допустимого значения для типа int (определённого в <strong>PHP_INT_MAX</strong>), то она приводится к типу int. Иначе она приводится к типу float.</li><li>Если в заданном контексте дозволительно использовать префиксную числовую строку, то, если начало строки представляет целое число и не превышает максимально допустимого значения для типа int (определённого в <strong>PHP_INT_MAX</strong>), то она приводится к типу int. Иначе она приводится к типу float. Также, в этом случае, выдаётся ошибка уровня <strong>E_WARNING</strong>.</li><li>Если строка не числовая - выбрасывается исключение <a href="https://www.php.net/manual/ru/class.typeerror.php">TypeError</a>.</li></ol><div><br></div>	t
88	Массивы	massivy	70	<div><a href="https://www.php.net/manual/ru/language.types.array.php">Массивы</a><br>На самом деле массив в PHP - это упорядоченное отображение, которое устанавливает соответствие между <em>значением</em> и <em>ключом</em>. Этот тип оптимизирован в нескольких направлениях, поэтому вы можете использовать его как собственно массив, список (вектор), хеш-таблицу (являющуюся реализацией карты), словарь, коллекцию, стек, очередь и, возможно, что-то ещё. Так как значением массива может быть другой массив PHP, можно также создавать деревья и многомерные массивы.</div><div><br></div>	t
89	EXCEPTION	exception	\N	<div><strong>Exception</strong> — это базовый класс для всех пользовательских исключений.<br>Модель исключений (exceptions) в PHP похожа с используемыми в других языках программирования. Исключение можно сгенерировать (выбросить) при помощи оператора <a href="https://www.php.net/manual/ru/language.exceptions.php">throw</a>, и можно перехватить (поймать) оператором <a href="https://www.php.net/manual/ru/language.exceptions.php#language.exceptions.catch">catch</a>. Код генерирующий исключение, должен быть окружён блоком <a href="https://www.php.net/manual/ru/language.exceptions.php">try</a>, для того, чтобы можно было перехватить исключение. Каждый блок <a href="https://www.php.net/manual/ru/language.exceptions.php">try</a> должен иметь как минимум один соответствующий ему блок <a href="https://www.php.net/manual/ru/language.exceptions.php#language.exceptions.catch">catch</a> или <a href="https://www.php.net/manual/ru/language.exceptions.php#language.exceptions.finally">finally</a>.</div><div><br></div>	t
90	Итерируемые	iteriruemye	70	<div><a href="https://www.php.net/manual/ru/language.types.iterable.php">Итерируемые</a><br><a href="https://www.php.net/manual/ru/language.types.iterable.php">Iterable</a> - псевдотип, введённый в PHP 7.1. Он принимает любой массив (array) или объект, реализующий интерфейс <a href="https://www.php.net/manual/ru/class.traversable.php">Traversable</a>. Оба этих типа итерируются с помощью <a href="https://www.php.net/manual/ru/control-structures.foreach.php">foreach</a> и могут быть использованы с <strong>yield from</strong> в <a href="https://www.php.net/manual/ru/language.generators.php">генераторах</a>.<br><br></div><div>Использование Iterable<a href="https://www.php.net/manual/ru/language.types.iterable.php#language.types.iterable.using">&nbsp;</a></div><div>Тип iterable может использоваться как тип параметра для указания, что функция принимает набор значений, но ей не важна форма этого набора, пока он будет использоваться с <a href="https://www.php.net/manual/ru/control-structures.foreach.php">foreach</a>. Если значение не является массивом или объектом, реализующим <a href="https://www.php.net/manual/ru/class.traversable.php">Traversable</a>, будет выброшено исключение <a href="https://www.php.net/manual/ru/class.typeerror.php">TypeError</a>.<br><br></div><div><br></div><div><br></div>	t
91	Объекты	obekty	70	<div><a href="https://www.php.net/manual/ru/language.types.object.php">Объекты</a></div><div>Инициализация объекта<a href="https://www.php.net/manual/ru/language.types.object.php#language.types.object.init">&nbsp;</a></div><div>Для создания нового объекта, используйте выражение new, создающее в переменной экземпляр класса:<br><br></div><div>&lt;?php<br>class foo<br>{<br>&nbsp; &nbsp; function do_foo()<br>&nbsp; &nbsp; {<br>&nbsp; &nbsp; &nbsp; &nbsp; echo "Код foo.";<br>&nbsp; &nbsp; }<br>}<br><br>$bar = new foo;<br>$bar-&gt;do_foo();<br>?&gt;<br>Если object преобразуется в object, объект не изменится. Если значение другого типа преобразуется в object, создаётся новый экземпляр встроенного класса stdClass. Если значение было <strong>null</strong>, новый экземпляр будет пустым. Массивы преобразуются в object с именами полей, названными согласно ключам массива и соответствующими им значениям. Обратите внимание, что в этом случае до PHP 7.2.0 числовые ключи не будут доступны, пока не проитерировать объект.</div>	t
92	Перечисления	perechisleniia	70	<div><a href="https://www.php.net/manual/ru/language.types.enumerations.php">Перечисления</a></div><div>Перечисления - это ограничивающий слой над классами и константами классов, предназначенный для предоставления способа определения закрытого набора возможных значений для типа.<br>&lt;?php<br>enum Suit<br>{<br>&nbsp; &nbsp; case Hearts;<br>&nbsp; &nbsp; case Diamonds;<br>&nbsp; &nbsp; case Clubs;<br>&nbsp; &nbsp; case Spades;<br>}<br><br>function do_stuff(Suit $s)<br>{<br>&nbsp; &nbsp; // ...<br>}<br><br>do_stuff(Suit::Spades);<br>?&gt;<br>Если перечисление (enum) преобразуется в объект (object), оно не изменяется. Если перечисление (enum) преобразуется в массив (array), массив с одним ключом name (для простых перечислений) или массив с обоими ключами name и value (для типизированных перечислений). Все остальные приведения типов приведут к ошибке.</div><div><br></div>	t
93	Ресурс	resurs	70	<div><a href="https://www.php.net/manual/ru/language.types.resource.php">Ресурс</a><br>Resource - это специальная переменная, содержащая ссылку на внешний ресурс. Ресурсы создаются и используются специальными функциями. Полный перечень этих функций и соответствующих типов ресурсов (resource) смотрите в <a href="https://www.php.net/manual/ru/resource.php">приложении</a>.<br><br></div><div>Смотрите также описание функции <a href="https://www.php.net/manual/ru/function.get-resource-type.php">get_resource_type()</a>.<br><br></div><div>Преобразование в ресурс<a href="https://www.php.net/manual/ru/language.types.resource.php#language.types.resource.casting">&nbsp;</a></div><div>Поскольку тип resource содержит специальные указатели на открытые файлы, соединения с базой данных, области изображения и тому подобное, преобразование в этот тип не имеет смысла.<br><br></div><div><br></div>	t
94	NULL	null	70	<div><a href="https://www.php.net/manual/ru/language.types.null.php">NULL</a><br>Специальное значение <strong>null</strong> представляет собой переменную без значения. <strong>null</strong> - это единственно возможное значение типа null.<br><br></div><div>Переменная считается null, если:<br><br></div><ul><li>ей была присвоена константа <strong>null</strong>.</li><li>ей ещё не было присвоено никакого значения.</li><li>она была удалена с помощью <a href="https://www.php.net/manual/ru/function.unset.php">unset()</a>.<br><br></li></ul><div>Синтаксис<a href="https://www.php.net/manual/ru/language.types.null.php#language.types.null.syntax">&nbsp;</a></div><div>Существует только одно значение типа null - регистронезависимая константа <strong>null</strong>.<br><br></div><div><br></div>	t
96	Объявление типов	obiavlenie-tipov	70	<div><a href="https://www.php.net/manual/ru/language.types.declarations.php">Объявление типов</a><br>Объявления типов могут использоваться для аргументов функций, возвращаемых значений и, начиная с PHP 7.4.0, для свойств класса. Они используются во время исполнения для проверки, что значение имеет точно тот тип, который для них указан. В противном случае будет выброшено исключение <a href="https://www.php.net/manual/ru/class.typeerror.php">TypeError</a>.<br><br></div><blockquote><strong>Замечание</strong>:При переопределении родительского метода, тип возвращаемого значения дочернего метода должен соответствовать любому объявлению возвращаемого типа родительского. Если в родительском методе тип возвращаемого значения не объявлен, то это можно сделать в дочернем.</blockquote><div><br></div>	t
97	Манипуляции с типами	manipuliatsii-s-tipami	70	<div><a href="https://www.php.net/manual/ru/language.types.type-juggling.php">Манипуляции с типами</a><br>PHP не требует явного определения типа при объявлении переменной. В этом случае тип переменной определяется значением, которое она хранит. То есть, если переменной $var присваивается значение типа строка (string), то $var изменит тип на строку (string). Если после этого переменной $var будет присвоено значение типа целое число (int), то она изменит тип на целое число (int).<br><br></div><div>В определённых контекстах PHP может попытаться автоматически преобразовать тип значения в другой. Существуют следующие различные контексты:<br><br></div><ul><li>Числовой контекст</li><li>Строчный контекст</li><li>Логический контекст</li><li>Контекст целых чисел и строк</li><li>Сравнительный контекст</li><li>Контекст функций</li></ul><div><br></div><blockquote><strong>Замечание</strong>: Когда значение нужно интерпретировать как другой тип, само значение <em>не</em> меняет тип.</blockquote><div><br>Чтобы принудительно установить тип переменной, смотрите раздел <a href="https://www.php.net/manual/ru/language.types.type-juggling.php#language.types.typecasting">Приведение типа</a>. Чтобы изменить тип переменной, смотрите описание функции <a href="https://www.php.net/manual/ru/function.settype.php">settype()</a>.<br><br></div><div><br></div>	t
\.


--
-- Name: menu_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.menu_id_seq', 97, true);


--
-- Name: doctrine_migration_versions doctrine_migration_versions_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.doctrine_migration_versions
    ADD CONSTRAINT doctrine_migration_versions_pkey PRIMARY KEY (version);


--
-- Name: menu menu_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.menu
    ADD CONSTRAINT menu_pkey PRIMARY KEY (id);


--
-- Name: idx_7d053a93727aca70; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX idx_7d053a93727aca70 ON public.menu USING btree (parent_id);


--
-- Name: menu fk_7d053a93727aca70; Type: FK CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.menu
    ADD CONSTRAINT fk_7d053a93727aca70 FOREIGN KEY (parent_id) REFERENCES public.menu(id);


--
-- PostgreSQL database dump complete
--

