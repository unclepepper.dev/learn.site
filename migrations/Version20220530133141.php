<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220530133141 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE menu ADD parent INT DEFAULT NULL');
        $this->addSql('ALTER TABLE menu ADD content TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE menu ADD is_visible BOOLEAN NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE menu DROP parent');
        $this->addSql('ALTER TABLE menu DROP content');
        $this->addSql('ALTER TABLE menu DROP is_visible');
    }
}
