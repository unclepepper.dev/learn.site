<?php

declare(strict_types=1);

namespace App\Doctrine\Filter;

use App\Entity\Menu;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Query\Filter\SQLFilter;

class VisibleFilter extends SQLFilter
{
    public function addFilterConstraint(ClassMetadata $targetEntity, $targetTableAlias): string
    {
        if (Menu::class == !$targetEntity->getName()) {
            return '';
        }

        return \sprintf('%s.is_visible is true', $targetTableAlias);
    }
}
