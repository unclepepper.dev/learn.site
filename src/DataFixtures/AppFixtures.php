<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Menu;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use function sprintf;

class AppFixtures extends Fixture
{
    private const CONTENT = 'This description has information about %s paragraph, %s item and %s child';
    private const FIELD_NAME = 'пункт %s %s %s';

    public function load(ObjectManager $manager): void
    {
        for ($i = 1; $i < 7; ++$i) {
            $parent = $this->createParent(sprintf(self::FIELD_NAME, $i, '', ''));
            $parent->setContent(sprintf(self::CONTENT, $i, '', ''));

            for ($j = 1; $j < 3; ++$j) {
                $item = $this->createPhpItem($parent, sprintf(self::FIELD_NAME, $i, $j, ''));
                $item->setContent(sprintf(self::CONTENT, $i, $j, ''));

                for ($s = 1; $s < 4; ++$s) {
                    $child = $this->createPhpItem($item, sprintf(self::FIELD_NAME, $i, $j, $s));
                    $child->setContent(sprintf(self::CONTENT, $i, $j, $s));
                    $manager->persist($child);
                }
                $manager->persist($item);
            }
            $manager->persist($parent);
        }
        $manager->flush();
    }

    private function createParent(string $name): Menu
    {
        $menu = new Menu();

        return $menu
            ->setName($name)
            ->setIsVisible(false);
    }

    private function createPhpItem(Menu $parent, string $nameItem): Menu
    {
        $menu = new Menu();

        return $menu
            ->setName($nameItem)
            ->setIsVisible(false)
            ->setParent($parent);
    }
}
