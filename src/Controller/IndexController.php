<?php

declare(strict_types=1);

namespace App\Controller;

use App\Repository\MenuRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{
    public function __construct(
        private MenuRepository $doctrine,
    ) {
    }

    #[Route('/', name: 'app_index')]
    public function index(): Response
    {
        $items = $this->doctrine->findAllField();

        return $this->render('index/index.html.twig', [
            'items' => $items,
        ]);
    }
}
