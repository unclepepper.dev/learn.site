<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Menu;
use App\Repository\MenuRepository;
use App\Service\GetParentVisibleService;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ItemController extends AbstractController
{
    public function __construct(
        private MenuRepository $repository,
        private GetParentVisibleService $service
    ) {
    }

    /**
     * @throws NonUniqueResultException
     */
    #[Route('/{slug}', name: 'app_item')]
    public function index($slug): Response
    {
        /** @var Menu|null $parent */
        $parent = $this->repository->findOneBySlugField($slug);

        if (null !== $parent) {
            $items = $parent->getMenus();

            if ($this->service->getVisibleParent($parent)) {
                return $this->render('item/index.html.twig', [
                    'items' => $items,
                    'parent' => $parent,
                ]);
            }
        }

        return $this->redirectToRoute('app_index');
    }
}
