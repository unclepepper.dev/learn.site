<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Menu;

class GetParentVisibleService
{
    public function getVisibleParent(Menu $item): bool
    {
        if ($item->getParent()) {
            if (!$item->getParent()->isIsVisible()) {
                return false;
            }
        }

        return true;
    }
}
